#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

"""          Example DAG demonstrating the DummyOperator and a custom DummySkipOperator which skips by default."""

from airflow import DAG
from airflow.exceptions import AirflowSkipException
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

args = {
    'owner': 'sushil',
    'start_date': days_ago(2),
}


def create_test_pipeline(suffix, trigger_rule, dag_):
    """
    Instantiate a number of operators for the given DAG.

    :param str suffix: Suffix to append to the operator task_ids
    :param str trigger_rule: TriggerRule for the join task
    :param DAG dag_: The DAG to run the operators on
    """
    
    sample_bash_1 = BashOperator(task_id='bash_operator_{}'.format(suffix), bash_command='more code/sn_test1.py', dag=dag_)
    sample_bash_2 = BashOperator(task_id='bash_operator_{}'.format(suffix), bash_command='more code/sn_test2.py', dag=dag_)
    final = DummyOperator(task_id='final_{}'.format(suffix), dag=dag_)

    sample_bash_1 >> sample_bash_2 >> final

dag = DAG(dag_id='example_skip_dag_sn', default_args=args, tags=['example'])

code_path = 'my-repo/wsl-appd-practice/code'

create_test_pipeline('1', 'all_success', dag)
create_test_pipeline('2', 'one_success', dag)

